Feature: Alert Action
  As A user
  I want to setup alerts
  So That i can get notifications when events are triggered

  Scenario: create alert action
    Given i am on the home page
    And i navigate to status page
    And click on alerting
    And click add action
    And confirm action enabled
    And confirm send alert to email
    And select alert method
    And select alert on type
    And select alert on severity
    And select alert when
    And select alert period
    And click options to alert on
    When i click save
    Then a new alert action is created
