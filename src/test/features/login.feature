
Feature: Login functionality
  As A valid user
  I want to login to highlight
  So That i can access highlight features
  @valid
  Scenario: valid user credentials
    Given i am on login page
    When i enter valid username and password and click login button
    Then i should be navigated to the home page

  Scenario: invalid email login
    Given i am on login page
    When i enter invalid username and valid password and click login button
    Then i should see an error message

  Scenario: invalid password login
    Given i am on login page
    When i enter valid username and invalid password and click login button
    Then i should see an error message

  Scenario: invalid email and password login
    Given i am on login page
    When i enter invalid username and invalid password and click login button
    Then i should see an error message


