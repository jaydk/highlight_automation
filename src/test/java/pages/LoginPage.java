package pages;

import base.TestBase;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends TestBase {

WebDriverWait wait = new WebDriverWait(driver, 10);

  @FindBy(id = "Email")
  @CacheLookup
  private WebElement email;

  @FindBy(id = "Password")
  @CacheLookup
  private WebElement password;

  @FindBy(id = "loginPanel_loginButton")
  @CacheLookup
  private WebElement loginButton;

  @FindBy(xpath = "//span[@class='field-validation-error']")
  @CacheLookup
  private WebElement invalidLoginErrorMessage;

  public void validUserLogin(){
   // wait.until(ExpectedConditions.elementToBeClickable(email));
  email.sendKeys(userdata.getProperty("validEmail"));
  password.sendKeys(userdata.getProperty("validPassword"));
  loginButton.click();

  }
  public void invalidEmailLogin(){
    email.sendKeys(userdata.getProperty("invalidEmail"));
    password.sendKeys(userdata.getProperty("validPassword"));
    loginButton.click();
  }
  public void invalidPasswordLogin(){
    email.sendKeys(userdata.getProperty("validEmail"));
    password.sendKeys(userdata.getProperty("invalidPassword"));
    loginButton.click();
  }
  public void invalidEmailAndInvalidPasswordLogin(){
    email.sendKeys(userdata.getProperty("invalidEmail"));
    password.sendKeys(userdata.getProperty("invalidPassword"));
    loginButton.click();
  }
  public void invalidCredentialsErrorMessage(){
    String errMsg = invalidLoginErrorMessage.getText();
    System.out.println("Error Message Displayed: "+ errMsg);
    Assert.assertEquals("Email or password is incorrect",errMsg);
    Assert.assertTrue(invalidLoginErrorMessage.isDisplayed());
  }

  public void verifyHomePage(){
    validUserLogin();
    wait.until(ExpectedConditions.visibilityOf(driver.getTitle()));
    String title = driver.getTitle();
    System.out.println("Page title: "+ title );
    Assert.assertTrue(title.equalsIgnoreCase("Home - Highlight"));
  }

}
