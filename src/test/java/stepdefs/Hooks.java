package hooks;

import base.TestBase;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;

import static base.TestBase.initializeDriver;

public class Hooks {

  private WebDriver driver;

  @Before
  public WebDriver driverinit(){
    initializeDriver();
    return driver;
  }

  @After
  public void tearDown(){
    driver.quit();
  }
}
