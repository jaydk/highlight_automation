package stepdefs;

import base.TestBase;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.PageFactory;
import pages.LoginPage;

public class LoginPageSteps extends TestBase {
  LoginPage loginPage;

  public LoginPageSteps() {
    initializeDriver();
    loginPage = new LoginPage();
    PageFactory.initElements(driver, loginPage);
  }

  @Given("i am on login page")
  public void i_am_on_login_page() {
    openLoginPage();
  }

  @When("i enter valid username and password and click login button")
  public void i_enter_valid_username_password_and_click_login_button() {
    loginPage.validUserLogin();
  }

  @Then("i should be navigated to the home page")
  public void i_should_be_navigated_to_the_home_page() {
    loginPage.verifyHomePage();
  }

  @When("i enter invalid username and valid password and click login button")
  public void i_enter_invalid_username_and_valid_password_and_click_login_button() {
    loginPage.invalidEmailLogin();
  }

  @When("i enter valid username and invalid password and click login button")
  public void i_enter_valid_username_and_invalid_password_and_click_login_button() {
    loginPage.invalidPasswordLogin();
  }

  @When("i enter invalid username and invalid password and click login button")
  public void i_enter_invalid_username_and_invalid_password_and_click_login_button() {
    loginPage.invalidEmailAndInvalidPasswordLogin();
  }

  @Then("i should see an error message")
  public void i_should_see_an_error_message() {
  loginPage.invalidCredentialsErrorMessage();
  }
}
